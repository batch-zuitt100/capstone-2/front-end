let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem('token')


fetch(`http://localhost:4000/api/courses/${courseId}`, {
		method: 'DELETE',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			courseId,
			// isActive: isActive
		})
	})
	.then(res => {
		return res.json()
	})
	.then(data => {
		console.log(data)
		//deletion course successful
		if(data === true) {
			//redirect to course page
			alert('Course succesfully disabled!')
			window.location.replace("./courses.html")
		} else {

			alert("Something went wrong.")

		}
	})



