let editCourse = document.querySelector('#editCourse');

editCourse.addEventListener('submit', (e) => {
	e.preventDefault()

	let name = document.querySelector('#courseName').value;
	let description = document.querySelector('#courseDescription').value;
	let price = document.querySelector('#coursePrice').value;

	let params = new URLSearchParams(window.location.search)
	let courseId = params.get('courseId')
	let token = localStorage.getItem('token')


	fetch(`http://localhost:4000/api/courses`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId,
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			//editing of old course successful
			if(data === true) {
				//redirect to course page
				alert('Course succesfully edited!')
				window.location.replace("./courses.html")
			} else {
				
				alert("Something went wrong.")

			}
		})
})