let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem('token')


// console.log(courseId)
let courseName = document.querySelector("#courseName")
let courseDescription = document.querySelector("#courseDescription")

// let enrollees = document.querySelector("#enrollContainer")


fetch(`http://localhost:4000/api/courses/${courseId}`)

.then(res => res.json())
.then(data => {
	console.log(data)

	courseName.innerHTML = data.name
	courseDescription.innerHTML = data.description
	// enrollees.innerHTML = data.enrollees

	let enrolleesData = data.enrollees
	let enrollmentsInfo = [];
	let enrollContainer = document.querySelector("#enrollContainer")

	for (let student of enrolleesData) {
		fetch(`http://localhost:4000/api/users/${student.userId}`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
				}
		})
		.then(res => res.json())
		.then(data =>{
			enrollContainer.innerHTML = "";
			enrollmentsInfo.push({firstName:data.firstName, lastName:data.lastName});

			for(let i = 0; i < enrollmentsInfo.length; i++){
				console.log(enrollmentsInfo[i].firstName)
				const newLi = document.createElement('li');
				newLi.append(`${enrollmentsInfo[i].firstName} ${enrollmentsInfo[i].lastName}`);
				enrollContainer.append(newLi);
			}
		})

	}
	if (enrollmentsInfo.length < 1) {
				enrollContainer.innerHTML = "No enrollees yet."

			}


})

// router.get('/:courseId', (req, res) => {
// 	const courseId = req.params.courseId
//     CourseController.get({ courseId }).then(course => res.send(course)) 
// })